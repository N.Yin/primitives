{
    "id": "d921be1e-b158-4ab7-abb3-cb1b17f42639",
    "version": "0.1.0",
    "name": "retina_net",
    "python_path": "d3m.primitives.object_detection.retina_net.ObjectDetectionRN",
    "keywords": [
        "object detection",
        "convolutional neural network",
        "digital image processing",
        "RetinaNet"
    ],
    "source": {
        "name": "Distil",
        "contact": "mailto:jeffrey.gleason@kungfu.ai",
        "uris": [
            "https://github.com/kungfuai/d3m-primitives"
        ]
    },
    "installation": [
        {
            "type": "PIP",
            "package": "cython",
            "version": "0.29.16"
        },
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/kungfuai/d3m-primitives.git@985acacd7e7eaf592f4ed5de480a37d42706a084#egg=kf-d3m-primitives"
        },
        {
            "type": "FILE",
            "key": "resnet50",
            "file_uri": "http://public.datadrivendiscovery.org/ResNet-50-model.keras.h5",
            "file_digest": "0128cdfa3963288110422e4c1a57afe76aa0d760eb706cda4353ef1432c31b9c"
        }
    ],
    "algorithm_types": [
        "RETINANET"
    ],
    "primitive_family": "OBJECT_DETECTION",
    "can_use_gpus": true,
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "primitives.object_detection.retinanet.object_detection_retinanet.ObjectDetectionRNPrimitive",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "primitives.object_detection.retinanet.object_detection_retinanet.Params",
            "Hyperparams": "primitives.object_detection.retinanet.object_detection_retinanet.Hyperparams"
        },
        "interfaces_version": "2020.5.18",
        "interfaces": [
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "backbone": {
                "type": "d3m.metadata.hyperparams.Union",
                "default": "resnet50",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Backbone architecture from which RetinaNet is built. All backbones require a weights file downloaded for use during runtime.",
                "configuration": {
                    "resnet50": {
                        "type": "d3m.metadata.hyperparams.Constant",
                        "default": "resnet50",
                        "structural_type": "str",
                        "semantic_types": [
                            "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                        ],
                        "description": "Backbone architecture from resnet50 architecture (https://arxiv.org/abs/1512.03385)"
                    }
                }
            },
            "batch_size": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 32,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Size of the batches as input to the model."
            },
            "n_epochs": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 10,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Number of epochs to train."
            },
            "freeze_backbone": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Freeze training of backbone layers."
            },
            "weights": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Load the model with pretrained weights specific to selected backbone."
            },
            "learning_rate": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 1e-05,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Learning rate."
            },
            "n_steps": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 20,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Number of steps/epoch."
            },
            "output": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": false,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Output images and predicted bounding boxes after evaluation."
            },
            "weights_path": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": "/root/",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "An output path for where model weights should be saved/loaded from during runtime"
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "primitives.object_detection.retinanet.object_detection_retinanet.Hyperparams",
                "kind": "RUNTIME"
            },
            "volumes": {
                "type": "typing.Union[NoneType, typing.Dict[str, str]]",
                "kind": "RUNTIME",
                "default": null
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "outputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "primitives.object_detection.retinanet.object_detection_retinanet.Params",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {},
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "volumes",
                    "random_seed"
                ],
                "returns": "NoneType"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "Creates the image generators and then trains RetinaNet model on the image paths in the input\ndataframe column.\n\nCan choose to use validation generator.\n\nIf no weight file is provided, the default is to use the ImageNet weights.\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "outputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nThis method allows primitive author to implement an optimized version of both fitting\nand producing a primitive on same data.\n\nIf any additional method arguments are added to primitive's ``set_training_data`` method\nor produce method(s), or removed from them, they have to be added to or removed from this\nmethod as well. This method should accept an union of all arguments accepted by primitive's\n``set_training_data`` method and produce method(s) and then use them accordingly when\ncomputing results.\n\nThe default implementation of this method just calls first ``set_training_data`` method,\n``fit`` method, and all produce methods listed in ``produce_methods`` in order and is\npotentially inefficient.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to ``set_training_data`` and all produce methods.\noutputs:\n    The outputs given to ``set_training_data``.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "primitives.object_detection.retinanet.object_detection_retinanet.Params",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nAn instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Produce image detection predictions.\n\nParameters\n----------\n    inputs  : numpy ndarray of size (n_images, dimension) containing the d3m Index, image name,\n              and bounding box for each image.\n\nReturns\n-------\n    outputs : A d3m dataframe container with the d3m index, image name, bounding boxes as\n              a string (8 coordinate format), and confidence scores."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams:\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs",
                    "outputs"
                ],
                "returns": "NoneType",
                "description": "Sets the primitive's training data and preprocesses the files for RetinaNet format.\n\nParameters\n----------\n    inputs: numpy ndarray of size (n_images, dimension) containing the d3m Index, image name,\n            and bounding box for each image.\n\nReturns\n-------\n    No returns. Function is called by pipeline at runtime."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {
            "base_dir": "str",
            "image_paths": "pandas.core.series.Series",
            "annotations": "pandas.core.frame.DataFrame",
            "classes": "pandas.core.frame.DataFrame"
        }
    },
    "structural_type": "primitives.object_detection.retinanet.object_detection_retinanet.ObjectDetectionRNPrimitive",
    "description": "Primitive that utilizes RetinaNet, a convolutional neural network (CNN), for object\ndetection. The methodology comes from \"Focal Loss for Dense Object Detection\" by\nLin et al. 2017 (https://arxiv.org/abs/1708.02002). The code implementation is based\noff of the base library found at: https://github.com/fizyr/keras-retinanet.\n\nThe primitive accepts a Dataset consisting of images, labels as input and returns\na dataframe as output which include the bounding boxes for each object in each image.\n\nAttributes\n----------\nmetadata:\n    Primitive's metadata. Available as a class attribute.\nlogger:\n    Primitive's logger. Available as a class attribute.\nhyperparams:\n    Hyperparams passed to the constructor.\nrandom_seed:\n    Random seed passed to the constructor.\ndocker_containers:\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes:\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory:\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "digest": "f35c347faacefc20292ffcfd7911d6ec0f5073fc17d87444efb70a5272f65bac"
}
